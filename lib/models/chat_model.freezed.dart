// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'chat_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ChatEntity _$ChatEntityFromJson(Map<String, dynamic> json) {
  return _ChatEntity.fromJson(json);
}

/// @nodoc
mixin _$ChatEntity {
  int get id => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;
  @JsonKey(name: "is_private")
  int get is_private => throw _privateConstructorUsedError;
  @JsonKey(name: "created_at")
  String get created_at => throw _privateConstructorUsedError;
  @JsonKey(name: "updated_at")
  String get updated_at => throw _privateConstructorUsedError;
  @JsonKey(name: "last_message")
  ChatMessageEntity? get last_message => throw _privateConstructorUsedError;
  List<ChatParticipantEntity> get participants =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ChatEntityCopyWith<ChatEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatEntityCopyWith<$Res> {
  factory $ChatEntityCopyWith(
          ChatEntity value, $Res Function(ChatEntity) then) =
      _$ChatEntityCopyWithImpl<$Res, ChatEntity>;
  @useResult
  $Res call(
      {int id,
      String? name,
      @JsonKey(name: "is_private") int is_private,
      @JsonKey(name: "created_at") String created_at,
      @JsonKey(name: "updated_at") String updated_at,
      @JsonKey(name: "last_message") ChatMessageEntity? last_message,
      List<ChatParticipantEntity> participants});

  $ChatMessageEntityCopyWith<$Res>? get last_message;
}

/// @nodoc
class _$ChatEntityCopyWithImpl<$Res, $Val extends ChatEntity>
    implements $ChatEntityCopyWith<$Res> {
  _$ChatEntityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = freezed,
    Object? is_private = null,
    Object? created_at = null,
    Object? updated_at = null,
    Object? last_message = freezed,
    Object? participants = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      is_private: null == is_private
          ? _value.is_private
          : is_private // ignore: cast_nullable_to_non_nullable
              as int,
      created_at: null == created_at
          ? _value.created_at
          : created_at // ignore: cast_nullable_to_non_nullable
              as String,
      updated_at: null == updated_at
          ? _value.updated_at
          : updated_at // ignore: cast_nullable_to_non_nullable
              as String,
      last_message: freezed == last_message
          ? _value.last_message
          : last_message // ignore: cast_nullable_to_non_nullable
              as ChatMessageEntity?,
      participants: null == participants
          ? _value.participants
          : participants // ignore: cast_nullable_to_non_nullable
              as List<ChatParticipantEntity>,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $ChatMessageEntityCopyWith<$Res>? get last_message {
    if (_value.last_message == null) {
      return null;
    }

    return $ChatMessageEntityCopyWith<$Res>(_value.last_message!, (value) {
      return _then(_value.copyWith(last_message: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_ChatEntityCopyWith<$Res>
    implements $ChatEntityCopyWith<$Res> {
  factory _$$_ChatEntityCopyWith(
          _$_ChatEntity value, $Res Function(_$_ChatEntity) then) =
      __$$_ChatEntityCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String? name,
      @JsonKey(name: "is_private") int is_private,
      @JsonKey(name: "created_at") String created_at,
      @JsonKey(name: "updated_at") String updated_at,
      @JsonKey(name: "last_message") ChatMessageEntity? last_message,
      List<ChatParticipantEntity> participants});

  @override
  $ChatMessageEntityCopyWith<$Res>? get last_message;
}

/// @nodoc
class __$$_ChatEntityCopyWithImpl<$Res>
    extends _$ChatEntityCopyWithImpl<$Res, _$_ChatEntity>
    implements _$$_ChatEntityCopyWith<$Res> {
  __$$_ChatEntityCopyWithImpl(
      _$_ChatEntity _value, $Res Function(_$_ChatEntity) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = freezed,
    Object? is_private = null,
    Object? created_at = null,
    Object? updated_at = null,
    Object? last_message = freezed,
    Object? participants = null,
  }) {
    return _then(_$_ChatEntity(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      is_private: null == is_private
          ? _value.is_private
          : is_private // ignore: cast_nullable_to_non_nullable
              as int,
      created_at: null == created_at
          ? _value.created_at
          : created_at // ignore: cast_nullable_to_non_nullable
              as String,
      updated_at: null == updated_at
          ? _value.updated_at
          : updated_at // ignore: cast_nullable_to_non_nullable
              as String,
      last_message: freezed == last_message
          ? _value.last_message
          : last_message // ignore: cast_nullable_to_non_nullable
              as ChatMessageEntity?,
      participants: null == participants
          ? _value._participants
          : participants // ignore: cast_nullable_to_non_nullable
              as List<ChatParticipantEntity>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ChatEntity implements _ChatEntity {
  _$_ChatEntity(
      {required this.id,
      required this.name,
      @JsonKey(name: "is_private") required this.is_private,
      @JsonKey(name: "created_at") required this.created_at,
      @JsonKey(name: "updated_at") required this.updated_at,
      @JsonKey(name: "last_message") this.last_message,
      required final List<ChatParticipantEntity> participants})
      : _participants = participants;

  factory _$_ChatEntity.fromJson(Map<String, dynamic> json) =>
      _$$_ChatEntityFromJson(json);

  @override
  final int id;
  @override
  final String? name;
  @override
  @JsonKey(name: "is_private")
  final int is_private;
  @override
  @JsonKey(name: "created_at")
  final String created_at;
  @override
  @JsonKey(name: "updated_at")
  final String updated_at;
  @override
  @JsonKey(name: "last_message")
  final ChatMessageEntity? last_message;
  final List<ChatParticipantEntity> _participants;
  @override
  List<ChatParticipantEntity> get participants {
    if (_participants is EqualUnmodifiableListView) return _participants;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_participants);
  }

  @override
  String toString() {
    return 'ChatEntity(id: $id, name: $name, is_private: $is_private, created_at: $created_at, updated_at: $updated_at, last_message: $last_message, participants: $participants)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ChatEntity &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.is_private, is_private) ||
                other.is_private == is_private) &&
            (identical(other.created_at, created_at) ||
                other.created_at == created_at) &&
            (identical(other.updated_at, updated_at) ||
                other.updated_at == updated_at) &&
            (identical(other.last_message, last_message) ||
                other.last_message == last_message) &&
            const DeepCollectionEquality()
                .equals(other._participants, _participants));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      name,
      is_private,
      created_at,
      updated_at,
      last_message,
      const DeepCollectionEquality().hash(_participants));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ChatEntityCopyWith<_$_ChatEntity> get copyWith =>
      __$$_ChatEntityCopyWithImpl<_$_ChatEntity>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ChatEntityToJson(
      this,
    );
  }
}

abstract class _ChatEntity implements ChatEntity {
  factory _ChatEntity(
      {required final int id,
      required final String? name,
      @JsonKey(name: "is_private") required final int is_private,
      @JsonKey(name: "created_at") required final String created_at,
      @JsonKey(name: "updated_at") required final String updated_at,
      @JsonKey(name: "last_message") final ChatMessageEntity? last_message,
      required final List<ChatParticipantEntity> participants}) = _$_ChatEntity;

  factory _ChatEntity.fromJson(Map<String, dynamic> json) =
      _$_ChatEntity.fromJson;

  @override
  int get id;
  @override
  String? get name;
  @override
  @JsonKey(name: "is_private")
  int get is_private;
  @override
  @JsonKey(name: "created_at")
  String get created_at;
  @override
  @JsonKey(name: "updated_at")
  String get updated_at;
  @override
  @JsonKey(name: "last_message")
  ChatMessageEntity? get last_message;
  @override
  List<ChatParticipantEntity> get participants;
  @override
  @JsonKey(ignore: true)
  _$$_ChatEntityCopyWith<_$_ChatEntity> get copyWith =>
      throw _privateConstructorUsedError;
}
