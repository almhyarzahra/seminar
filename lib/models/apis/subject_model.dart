class SubjectModel {
  Data? data;
  bool? success;
  String? message;

  SubjectModel({this.data, this.success, this.message});

  SubjectModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    success = json['success'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['success'] = this.success;
    data['message'] = this.message;
    return data;
  }
}

class Data {
  int? id;
  String? name;
  String? email;
  int? category;
  int? age;
  int? year;
  int? isDoctor;
  Null photoPath;
  int? isAdmin;
  Null livestreamId;
  List<Subjects>? subjects;
  List<Doctors>? doctors;

  Data(
      {this.id,
      this.name,
      this.email,
      this.category,
      this.age,
      this.year,
      this.isDoctor,
      this.photoPath,
      this.isAdmin,
      this.livestreamId,
      this.subjects,
      this.doctors});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    category = json['category'];
    age = json['age'];
    year = json['year'];
    isDoctor = json['isDoctor'];
    photoPath = json['photo_path'];
    isAdmin = json['isAdmin'];
    livestreamId = json['livestream_id'];
    if (json['subjects'] != null) {
      subjects = <Subjects>[];
      json['subjects'].forEach((v) {
        subjects!.add(new Subjects.fromJson(v));
      });
    }
    if (json['doctors'] != null) {
      doctors = <Doctors>[];
      json['doctors'].forEach((v) {
        doctors!.add(new Doctors.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['category'] = this.category;
    data['age'] = this.age;
    data['year'] = this.year;
    data['isDoctor'] = this.isDoctor;
    data['photo_path'] = this.photoPath;
    data['isAdmin'] = this.isAdmin;
    data['livestream_id'] = this.livestreamId;
    if (this.subjects != null) {
      data['subjects'] = this.subjects!.map((v) => v.toJson()).toList();
    }
    if (this.doctors != null) {
      data['doctors'] = this.doctors!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Subjects {
  int? id;
  int? year;
  int? duration;
  String? name;
  String? specialty;
  int? category;
  String? department;
  String? laboratory;
  int? dR;
  Pivot? pivot;
  List<SubjectTimeSchedule>? subjectTimeSchedule;

  Subjects(
      {this.id,
      this.year,
      this.duration,
      this.name,
      this.specialty,
      this.category,
      this.department,
      this.laboratory,
      this.dR,
      this.pivot,
      this.subjectTimeSchedule});

  Subjects.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    year = json['year'];
    duration = json['duration'];
    name = json['name'];
    specialty = json['specialty'];
    category = json['category'];
    department = json['department'];
    laboratory = json['laboratory'];
    dR = json['DR'];
    pivot = json['pivot'] != null ? new Pivot.fromJson(json['pivot']) : null;
    if (json['subject_time_schedule'] != null) {
      subjectTimeSchedule = <SubjectTimeSchedule>[];
      json['subject_time_schedule'].forEach((v) {
        subjectTimeSchedule!.add(new SubjectTimeSchedule.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['year'] = this.year;
    data['duration'] = this.duration;
    data['name'] = this.name;
    data['specialty'] = this.specialty;
    data['category'] = this.category;
    data['department'] = this.department;
    data['laboratory'] = this.laboratory;
    data['DR'] = this.dR;
    if (this.pivot != null) {
      data['pivot'] = this.pivot!.toJson();
    }
    if (this.subjectTimeSchedule != null) {
      data['subject_time_schedule'] =
          this.subjectTimeSchedule!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Pivot {
  int? userId;
  int? subjectId;
  String? createdAt;
  String? updatedAt;
  int? passed;

  Pivot(
      {this.userId,
      this.subjectId,
      this.createdAt,
      this.updatedAt,
      this.passed});

  Pivot.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    subjectId = json['subject_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    passed = json['passed'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['subject_id'] = this.subjectId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['passed'] = this.passed;
    return data;
  }
}

class SubjectTimeSchedule {
  int? id;
  int? subjectId;
  String? day;
  String? begin;
  String? end;
  String? createdAt;
  String? updatedAt;

  SubjectTimeSchedule(
      {this.id,
      this.subjectId,
      this.day,
      this.begin,
      this.end,
      this.createdAt,
      this.updatedAt});

  SubjectTimeSchedule.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    subjectId = json['subject_id'];
    day = json['day'];
    begin = json['begin'];
    end = json['end'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['subject_id'] = this.subjectId;
    data['day'] = this.day;
    data['begin'] = this.begin;
    data['end'] = this.end;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Doctors {
  int? id;
  String? name;
  String? email;
  Null category;
  int? age;
  Null year;
  int? isDoctor;
  Null photoPath;
  int? isAdmin;
  Null livestreamId;

  Doctors(
      {this.id,
      this.name,
      this.email,
      this.category,
      this.age,
      this.year,
      this.isDoctor,
      this.photoPath,
      this.isAdmin,
      this.livestreamId});

  Doctors.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    category = json['category'];
    age = json['age'];
    year = json['year'];
    isDoctor = json['isDoctor'];
    photoPath = json['photo_path'];
    isAdmin = json['isAdmin'];
    livestreamId = json['livestream_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['category'] = this.category;
    data['age'] = this.age;
    data['year'] = this.year;
    data['isDoctor'] = this.isDoctor;
    data['photo_path'] = this.photoPath;
    data['isAdmin'] = this.isAdmin;
    data['livestream_id'] = this.livestreamId;
    return data;
  }
}
