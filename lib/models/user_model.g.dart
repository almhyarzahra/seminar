// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UserModel _$$_UserModelFromJson(Map<String, dynamic> json) => _$_UserModel(
      id: json['id'] as int?,
      photo_path: json['photo_path'] as String? ?? '',
      number: json['number'] as int? ?? 2,
      password: json['password'] as String? ?? 's',
      name: json['name'] as String,
      email: json['email'] as String,
      category: json['category'] as int? ?? 1,
      age: json['age'] as int? ?? 1,
      year: json['year'] as int? ?? 1,
      isDoctor: json['isDoctor'] as int? ?? 0,
      livestream_id: json['livestream_id'] as int?,
      isAdmin: json['isAdmin'] as int? ?? 0,
    );

Map<String, dynamic> _$$_UserModelToJson(_$_UserModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'photo_path': instance.photo_path,
      'number': instance.number,
      'password': instance.password,
      'name': instance.name,
      'email': instance.email,
      'category': instance.category,
      'age': instance.age,
      'year': instance.year,
      'isDoctor': instance.isDoctor,
      'livestream_id': instance.livestream_id,
      'isAdmin': instance.isAdmin,
    };

_$_AuthUser _$$_AuthUserFromJson(Map<String, dynamic> json) => _$_AuthUser(
      user: UserModel.fromJson(json['user'] as Map<String, dynamic>),
      token: json['token'] as String,
    );

Map<String, dynamic> _$$_AuthUserToJson(_$_AuthUser instance) =>
    <String, dynamic>{
      'user': instance.user,
      'token': instance.token,
    };
