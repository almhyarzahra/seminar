import 'dart:ffi';

import 'package:dash_chat_2/dash_chat_2.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:school_erp/models/User.dart';

part 'user_model.freezed.dart';

part 'user_model.g.dart';

@freezed
class UserModel with _$UserModel {
  UserModel._();

  factory UserModel(
      {int? id,
      @Default('') String photo_path,
      @Default(2) int number,
      @Default('s') String password,
      required String name,
      required String email,
      @Default(1) int category,
      @Default(1) int age,
      @Default(1) int year,
      @Default(0) int isDoctor,
      int? livestream_id,
      @Default(0) int isAdmin}) = _UserModel;

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  ChatUser get getChatUser {
    return ChatUser(id: id.toString(), firstName: name);
  }
}

@freezed
class AuthUser with _$AuthUser {
  const factory AuthUser({required UserModel user, required String token}) =
      _AuthUser;

  factory AuthUser.fromJson(Map<String, dynamic> json) =>
      _$AuthUserFromJson(json);
}
