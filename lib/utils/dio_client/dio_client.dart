import 'dart:io';

import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:school_erp/utils/dio_client/dart_interseptors.dart';

class DioClient {
  static DioClient? _singleton;
  static late Dio _dio;

  Dio get instance => _dio;

  DioClient._() {
    _dio = creatDioClient();
  }

  factory DioClient() {
    return _singleton ??= DioClient._();
  }

  Dio creatDioClient() {
    final dio = new Dio();
    dio.options.baseUrl = 'https://c2a9-185-107-56-146.ngrok-free.app/';
    dio.options.headers['accept'] = 'Application/Json';
    dio.options.headers['content-type'] = 'Application/Json';
    // dio.interceptors.add(AppInterceptors());
    // dio.options.headers[HttpHeaders.authorizationHeader]='bear'
    return dio;
  }
}
