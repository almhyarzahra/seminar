import '../models/models.dart';

String getChatName(
    List<ChatParticipantEntity> participants, UserModel currentUser) {
  final otherParticipant = participants
      .where((element) => element.userId != currentUser.id)
      .toList();
  if (otherParticipant.isNotEmpty) return otherParticipant[0].user.name;
  return "None";
}
