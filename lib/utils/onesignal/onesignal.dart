
import 'package:onesignal_flutter/onesignal_flutter.dart';

const oneSignalAppId="69a10cfe-3f5f-4196-a590-aceeb6101ba8";
Future<void> initOneSignal()async
{
final oneSignalShared=OneSignal.shared;
oneSignalShared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);
oneSignalShared.setRequiresUserPrivacyConsent(true);
await oneSignalShared.setAppId(oneSignalAppId);



}


registerOneSignalEventListener({

  required Function (OSNotificationOpenedResult) onOpened,
required Function (OSNotificationReceivedEvent) onReceivedInForeground
})
{
  final  oneSignalShared =OneSignal.shared;
  oneSignalShared.setNotificationOpenedHandler(onOpened);
oneSignalShared.setNotificationWillShowInForegroundHandler(onReceivedInForeground);

}

const tagName="useId";
sendUserTag(int userId)
{


  
}