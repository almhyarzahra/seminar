import 'package:get/get.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:school_erp/enums/message_type.dart';
import 'package:school_erp/models/apis/subject_model.dart';
import 'package:school_erp/repositories/repositories.dart';
import 'package:school_erp/services/base_controller.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_toast.dart';
import 'package:school_erp/ui/view/home_student_view/home_student_view.dart';
import 'package:school_erp/utils/logger.dart';

class HomeStudentController extends BaseController {
  Rx<SubjectModel> subjectModel = SubjectModel().obs;

  RxList<Subjects> subjects = <Subjects>[].obs;
  RxList<SubjectTimeSchedule> subjecTimeSchedule = <SubjectTimeSchedule>[].obs;
  RxMap<int, String> substance = <int, String>{}.obs;
  RxMap<int, String> department = <int, String>{}.obs;
  RxMap<int, String> laboratory = <int, String>{}.obs;
  Rx<DateTime> date = DateTime.now().obs;

  @override
  void onInit() async {
    await getAllSubject();

    super.onInit();
  }

  Map getSubjectToday() {
    Rx<String> dateFormat =
        DateFormat('EEEE').format(date.value).toLowerCase().obs;
    subjects =
        subjectModel.value.data?.subjects?.obs ?? RxList<Subjects>()().obs;

    for (int i = 0; i < subjects.length; i++) {
      subjecTimeSchedule.addAll(subjects[i].subjectTimeSchedule!.map((e) {
        if (e.day == dateFormat.value) {
          return e;
        } else
          return SubjectTimeSchedule();
      }).toList());
    }
    subjecTimeSchedule.removeWhere((element) => element.day == null);
    subjecTimeSchedule.sort((a, b) => b.begin!.compareTo(a.begin!));
    for (int i = 0; i < subjects.length; i++)
      for (int j = 0; j < subjecTimeSchedule.length; j++)
        if (subjects[i].id == subjecTimeSchedule[j].subjectId) {
          if (subjecTimeSchedule[j].begin == "8") {
            substance.addAll({8: subjects[i].name!.toCapitalized()});
            department.addAll({8: subjects[i].department!.toCapitalized()});
            laboratory.addAll({8: subjects[i].laboratory!});
          } else if (subjecTimeSchedule[j].begin == "10") {
            substance.addAll({10: subjects[i].name!.toCapitalized()});
            department.addAll({10: subjects[i].department!.toCapitalized()});
            laboratory.addAll({10: subjects[i].laboratory!});
          } else if (subjecTimeSchedule[j].begin == "12") {
            substance.addAll({12: subjects[i].name!.toCapitalized()});
            department.addAll({12: subjects[i].department!.toCapitalized()});
            laboratory.addAll({12: subjects[i].laboratory!});
          }
        }

    return substance;
  }

  int getTotalSubjects() {
    subjects =
        subjectModel.value.data?.subjects?.obs ?? RxList<Subjects>()().obs;
    return subjects.length;
  }

  Map getLaboratory() {
    return laboratory;
  }

  Map getDepartment() {
    return department;
  }

  String getState() {
    Rx<String> dateFormat =
        DateFormat('EEEE').format(date.value).toLowerCase().obs;
    if (dateFormat.value == "friday" || dateFormat.value == "saturday")
      return "Today is your holiday,";
    else
      return "Here is your activity today,";
  }

  Future<void> getAllSubject() async {
    runLoadingFutureFunction(
        function: UserRepository().getSubjectUser().then((value) {
      value.fold((l) {
        CustomToast.showMessage(
            message: "Wrong acsess internet",
            messageType: MessageType.REJECTED);
      }, (r) {
        subjectModel.value = r;
      });
    }));
  }
}
