import 'package:flutter/material.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';
import 'package:shimmer/shimmer.dart';

class HomeStudentViewShimmer extends StatefulWidget {
  const HomeStudentViewShimmer({super.key});

  @override
  State<HomeStudentViewShimmer> createState() => _HomeStudentViewShimmerState();
}

class _HomeStudentViewShimmerState extends State<HomeStudentViewShimmer> {
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
        child: Padding(
          padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(17)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              (screenWidth(15)).ph,
              Container(
                width: screenWidth(1.5),
                height: screenWidth(12),
                color: AppColors.mainBlackColor,
              ),
              (screenWidth(25)).ph,
              Container(
                width: screenWidth(2),
                height: screenWidth(20),
                color: AppColors.mainBlackColor,
              ),
              Padding(
                padding: EdgeInsetsDirectional.only(
                    top: screenWidth(20), bottom: screenWidth(20)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Item(),
                    Item(),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Item(),
                  Item(),
                ],
              ),
              Padding(
                padding: EdgeInsetsDirectional.only(top: screenWidth(10)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    CircleAvatar(
                      radius: screenWidth(12),
                    ),
                    CircleAvatar(
                      radius: screenWidth(12),
                    ),
                    CircleAvatar(
                      radius: screenWidth(12),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsetsDirectional.only(top: screenWidth(10)),
                child: Container(
                  width: screenWidth(1),
                  height: screenHeight(3.5),
                  decoration: BoxDecoration(
                      color: AppColors.mainBlackColor,
                      borderRadius:
                          BorderRadius.all(Radius.circular(screenWidth(30)))),
                ),
              ),
            ],
          ),
        ),
        baseColor: AppColors.mainGrey2Color,
        highlightColor: AppColors.mainGreyColor);
  }
}

Widget Item() {
  return Container(
    height: screenWidth(3.5),
    width: screenWidth(2.5),
    decoration: BoxDecoration(
        color: AppColors.mainGreyColor.withOpacity(0.3),
        borderRadius: BorderRadius.all(Radius.circular(screenWidth(30)))),
  );
}
