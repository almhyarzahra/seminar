import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:school_erp/enums/request_status.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/ui.dart';
import 'package:school_erp/ui/view/home_student_view/home_student_controller.dart';
import 'package:school_erp/ui/view/home_student_view/home_student_view_widgets/home_student_view_shimmer.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class HomeStudentView extends StatefulWidget {
  const HomeStudentView({super.key});

  @override
  State<HomeStudentView> createState() => _HomeStudentViewState();
}

class _HomeStudentViewState extends State<HomeStudentView> {
  HomeStudentController controller = Get.put(HomeStudentController());
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      body: Obx(() {
        return controller.requestStatus == RequestStatus.LOADING
            ? HomeStudentViewShimmer()
            : Padding(
                padding: EdgeInsetsDirectional.symmetric(
                    horizontal: screenWidth(17)),
                child: Column(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding:
                              EdgeInsetsDirectional.only(top: screenWidth(40)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Obx(() {
                                return CustomText(
                                  content:
                                      "Hi, ${controller.subjectModel.value.data?.name?.toCapitalized() ?? ''}",
                                  fontWeight: FontWeight.bold,
                                  fontSize: screenWidth(13),
                                  colorText: AppColors.mainBlackColor,
                                );
                              }),
                              IconButton(
                                  onPressed: () {
                                    Future.delayed(Duration(seconds: 5))
                                        .then((value) {
                                      Get.to(() => ChatListScreen());
                                    });
                                  },
                                  icon: SvgPicture.asset("images/bell.svg")),
                            ],
                          ),
                        ),
                        CustomText(
                          content: controller.getState(),
                          colorText: AppColors.mainGreyColor,
                        ),
                        Padding(
                          padding: EdgeInsetsDirectional.only(
                              top: screenWidth(20), bottom: screenWidth(20)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              ConItem(
                                  numberPer: "89%",
                                  text: "Presence",
                                  colorNumber: AppColors.mainRedColor),
                              ConItem(
                                  numberPer: "100%",
                                  text: "Completeness",
                                  colorNumber: AppColors.mainBlueColor),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ConItem(
                                numberPer: "18",
                                text: "Assigments",
                                colorNumber: AppColors.mainBlueColor),
                            ConItem(
                                numberPer: "${controller.getTotalSubjects()}",
                                text: "Total Subject",
                                colorNumber: AppColors.mainYellowColor),
                          ],
                        ),
                        Padding(
                          padding:
                              EdgeInsetsDirectional.only(top: screenWidth(10)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Column(
                                children: [
                                  IconButton(
                                      iconSize: screenWidth(7),
                                      onPressed: () {},
                                      icon: SvgPicture.asset(
                                          "images/course.svg")),
                                  CustomText(content: "Course"),
                                ],
                              ),
                              Column(
                                children: [
                                  IconButton(
                                      iconSize: screenWidth(7),
                                      onPressed: () {},
                                      icon: SvgPicture.asset(
                                          "images/subjects.svg")),
                                  CustomText(content: "Subjects"),
                                ],
                              ),
                              Column(
                                children: [
                                  IconButton(
                                      iconSize: screenWidth(7),
                                      onPressed: () {},
                                      icon: SvgPicture.asset(
                                          "images/presence.svg")),
                                  CustomText(content: "Presence"),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsetsDirectional.only(
                              top: screenWidth(30), bottom: screenWidth(20)),
                          child: CustomText(
                            content: "Schedule",
                            fontWeight: FontWeight.bold,
                            fontSize: screenWidth(15),
                          ),
                        ),
                        Container(
                          height: screenHeight(3.5),
                          child: ListView.builder(
                            itemCount: 15,
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (BuildContext context, int index) {
                              return index >= 8 && index % 2 == 0 && index != 14
                                  ? Column(
                                      children: [
                                        Stack(
                                          children: [
                                            Row(
                                              children: [
                                                index == 10 ||
                                                        index == 12 ||
                                                        index == 14
                                                    ? SizedBox.shrink()
                                                    : Column(
                                                        children: [
                                                          CustomText(
                                                              content:
                                                                  "$index"),
                                                          Container(
                                                            height:
                                                                screenWidth(2),
                                                            child:
                                                                VerticalDivider(
                                                              color: AppColors
                                                                  .mainGreyColor,
                                                              thickness: 1,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                (screenWidth(6)).pw,
                                                index == 14
                                                    ? SizedBox.shrink()
                                                    : Column(
                                                        children: [
                                                          CustomText(
                                                              content:
                                                                  "${index + 1}"),
                                                          Container(
                                                            height:
                                                                screenWidth(2),
                                                            child:
                                                                VerticalDivider(
                                                              color: AppColors
                                                                  .mainGreyColor,
                                                              thickness: 1,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                (screenWidth(6)).pw,
                                                index == 14
                                                    ? SizedBox.shrink()
                                                    : Column(
                                                        children: [
                                                          CustomText(
                                                              content:
                                                                  "${index + 2}"),
                                                          Container(
                                                            height:
                                                                screenWidth(2),
                                                            child:
                                                                VerticalDivider(
                                                              color: AppColors
                                                                  .mainGreyColor,
                                                              thickness: 1,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                              ],
                                            ),
                                            Padding(
                                              padding:
                                                  EdgeInsetsDirectional.only(
                                                      start: index == 10
                                                          ? 0
                                                          : screenWidth(35),
                                                      top: index == 10
                                                          ? screenWidth(5)
                                                          : index == 12
                                                              ? screenWidth(3.3)
                                                              : screenWidth(
                                                                  10)),
                                              child:
                                                  controller.getSubjectToday()[
                                                              index] ==
                                                          null
                                                      ? SizedBox.shrink()
                                                      : Container(
                                                          width:
                                                              screenWidth(2.5),
                                                          height:
                                                              screenWidth(10),
                                                          color: index == 8
                                                              ? AppColors
                                                                  .mainPurpleBackGroundColor
                                                                  .withOpacity(
                                                                      0.6)
                                                              : index == 10
                                                                  ? AppColors
                                                                      .mainBlueBackGroundColor
                                                                      .withOpacity(
                                                                          0.9)
                                                                  : AppColors
                                                                      .mainBlue100BackGroundColor
                                                                      .withOpacity(
                                                                          0.9),
                                                          child: Center(
                                                              child: Column(
                                                            children: [
                                                              CustomText(
                                                                content:
                                                                    "${controller.getSubjectToday()[index]}",
                                                                colorText: index ==
                                                                        8
                                                                    ? AppColors
                                                                        .mainYellowColor
                                                                    : index ==
                                                                            10
                                                                        ? AppColors
                                                                            .mainBlue10Color
                                                                        : AppColors
                                                                            .mainBlue20Color,
                                                              ),
                                                              Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .center,
                                                                children: [
                                                                  CustomText(
                                                                    content:
                                                                        "${controller.getDepartment()[index]}",
                                                                    fontSize:
                                                                        screenWidth(
                                                                            30),
                                                                    colorText:
                                                                        AppColors
                                                                            .mainGreyColor,
                                                                  ),
                                                                  (screenWidth(
                                                                          20))
                                                                      .pw,
                                                                  CustomText(
                                                                    content:
                                                                        "${controller.getLaboratory()[index]}",
                                                                    fontSize:
                                                                        screenWidth(
                                                                            35),
                                                                    colorText:
                                                                        AppColors
                                                                            .mainGreyColor,
                                                                  )
                                                                ],
                                                              )
                                                            ],
                                                          )),
                                                        ),
                                            )
                                          ],
                                        ),
                                      ],
                                    )
                                  : SizedBox.shrink();
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              );
      }),
    ));
  }

  Widget ConItem({
    required String numberPer,
    required String text,
    required Color colorNumber,
  }) {
    return Container(
      height: screenWidth(3.5),
      width: screenWidth(2.5),
      decoration: BoxDecoration(
          color: AppColors.mainGreyColor.withOpacity(0.3),
          borderRadius: BorderRadius.all(Radius.circular(screenWidth(30)))),
      child: Padding(
        padding: EdgeInsetsDirectional.only(start: screenWidth(15)),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(
                content: numberPer,
                fontWeight: FontWeight.bold,
                fontSize: screenWidth(12),
                colorText: colorNumber,
              ),
              CustomText(
                content: text,
                colorText: AppColors.mainBlackColor,
              ),
            ]),
      ),
    );
  }
}

extension StringCasingExtension on String {
  String toCapitalized() =>
      '${this[0].toUpperCase()}${substring(1).toLowerCase()}';
  String toTitleCase() => replaceAll(RegExp(' +'), ' ')
      .split(' ')
      .map((str) => str.toCapitalized())
      .join(' ');
}
