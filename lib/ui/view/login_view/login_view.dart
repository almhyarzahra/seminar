import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text_button.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/ui.dart';
import 'package:school_erp/ui/view/login_view/login_controller.dart';
import 'package:school_erp/ui/view/reset_password_view/reset_password_view.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';

class LoginView extends StatefulWidget {
  static const route_name = 'login_view';
  const LoginView({super.key});

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  LoginController controller = LoginController();
  // @override
  // void dispose() {
  //   controller.emailController.dispose();
  //   controller.passwordController.dispose();

  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.mainBlueColor,
      body: Form(
        key: controller.formKey,
        child: ListView(
          children: [
            Image.asset(
              "images/Coding workshop-rafiki.png",
              width: screenWidth(1),
              height: screenHeight(3),
            ),
            Padding(
              padding: EdgeInsetsDirectional.only(
                start: screenHeight(27),
              ),
              child: CustomText(
                content: "Hi Student",
                colorText: AppColors.mainBlackColor,
                fontSize: screenWidth(12),
                fontWeight: FontWeight.bold,
              ),
            ),
            Padding(
              padding: EdgeInsetsDirectional.only(
                  start: screenHeight(27), top: screenHeight(70)),
              child: CustomText(
                content: "Sign in to continue",
                colorText: AppColors.mainBlackColor,
                fontWeight: FontWeight.w300,
              ),
            ),
            (screenHeight(50)).ph,
            Container(
              height: screenHeight(1.8),
              decoration: BoxDecoration(
                  color: AppColors.mainWhiteColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(screenWidth(8)),
                    topRight: Radius.circular(screenWidth(8)),
                  )),
              child: Padding(
                padding: EdgeInsetsDirectional.symmetric(
                    horizontal: screenWidth(10)),
                child: Column(
                  children: [
                    CustomTextField(
                      controller: controller.emailController,
                      fontWeight: FontWeight.bold,
                      labelText: "Email",
                      validator: (value) {
                        return value!.isEmpty
                            ? "Please Check Your Email"
                            : null;
                      },
                    ),
                    (screenWidth(10)).ph,
                    Obx(() {
                      return CustomTextField(
                        controller: controller.passwordController,
                        labelText: "Password",
                        Obscure: controller.hidden.value,
                        fontWeight: FontWeight.bold,
                        letterSpacing: screenWidth(20),
                        suffixIcon: IconButton(
                            onPressed: () {
                              controller.replace();
                            },
                            icon: Icon(controller.hidden.isTrue
                                ? Icons.visibility_off_outlined
                                : Icons.visibility)),
                        validator: (value) {
                          return value!.isEmpty
                              // || !isPassword(value)
                              ? "Please Check Your Password"
                              : null;
                        },
                      );
                    }),
                    (screenWidth(10)).ph,
                    CustomButton(
                      onPressed: () {
                        controller.login(context);
                      },
                      Text: "SIGN IN",
                      suffixIcon: Icons.arrow_forward_sharp,
                      TextColor: AppColors.mainBlackColor,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        CustomTextButton(
                          onPressed: () {
                            Get.to(ResetPasswordView());
                          },
                          text: "Forget Password?",
                          colorText: AppColors.mainGreyColor,
                          decoration: TextDecoration.underline,
                          decorationThickness: 2,
                          fontSize: screenWidth(30),
                        ),
                      ],
                    ),
                    (screenWidth(5)).ph,
                    VirticalDivider(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
