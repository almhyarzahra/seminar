import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:school_erp/bloc/blocs.dart';
import 'package:school_erp/cubit/guest_cubit.dart';
import 'package:school_erp/enums/message_type.dart';
import 'package:school_erp/models/user_model.dart';
import 'package:school_erp/repositories/auth/auth_repository.dart';
import 'package:school_erp/services/base_controller.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_toast.dart';
import 'package:school_erp/ui/view/chat/chat_list.dart';
import 'package:school_erp/ui/view/home_view/home_view.dart';

class LoginController extends BaseController {
  TextEditingController emailController = TextEditingController();
  TextEditingController numberController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  RxBool hidden = true.obs;

  var cubit =
      GuestCubit(authBloc: AuthBloc(), authRrepository: AuthRepository());

  void replace() {
    hidden.isTrue ? hidden.value = false : hidden.value = true;
  }

  void login(BuildContext context) async {
    if (formKey.currentState!.validate()) {
      runFullLoadingFutureFunction(
          function: cubit
              .signIn(
                  context: context,
                  user: UserModel(
                      name: 'ibrahem',
                      number: 0996131104,
                      password: passwordController.text.toString(),
                      email: emailController.text.toString()))
              .then((value) {
        if (value == null) {
          Get.to(() => HomeView());
        } else
          CustomToast.showMessage(
              message: value, messageType: MessageType.REJECTED);
      })
          //       Future.delayed(Duration(seconds: 5))
          // .then((value) => Get.to(() => HomeView()));
          );
    }
  }
}
