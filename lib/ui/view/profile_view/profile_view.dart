import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text_field.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';

class ProfileView extends StatefulWidget {
  const ProfileView({super.key});

  @override
  State<ProfileView> createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  TextEditingController collegeNameController = TextEditingController();
  TextEditingController sectionController = TextEditingController();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController fatherNameController = TextEditingController();
  TextEditingController motherNameController = TextEditingController();
  TextEditingController idUniversityController = TextEditingController();
  TextEditingController academicYearController = TextEditingController();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColors.mainBlueColor,
      body: Form(
        key: _formKey,
        child: Stack(
          children: [
            Padding(
              padding: EdgeInsets.only(top: size.width * 0.08),
              child: SvgPicture.asset("images/star_pattern.svg"),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: size.width * 0.05),
              child: Padding(
                padding: EdgeInsets.only(top: size.width * 0.08),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        IconButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            icon: Icon(
                              Icons.arrow_back_ios,
                              color: AppColors.mainWhiteColor,
                            )),
                        CustomText(
                          content: "My Profile",
                          colorText: AppColors.mainWhiteColor,
                        ),
                      ],
                    ),
                    ElevatedButton.icon(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            setState(() {
                              isLoading = true;
                            });
                          }
                        },
                        style: ElevatedButton.styleFrom(
                            shape: StadiumBorder(),
                            primary: AppColors.mainWhiteColor),
                        icon: Icon(
                          Icons.check,
                          color: AppColors.mainBlueColor,
                        ),
                        label: CustomText(
                          content: "DONE",
                          colorText: AppColors.mainBlueColor,
                        ))
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: size.width * 0.3),
              child: Container(
                height: size.height * 0.2,
                width: size.width,
                decoration: BoxDecoration(
                    color: AppColors.mainWhiteColor,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(size.width * 0.1),
                      topRight: Radius.circular(size.width * 0.1),
                    )),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: size.width * 0.05,
                      vertical: size.width * 0.05),
                  child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: AppColors.mainGreyColor),
                        borderRadius: BorderRadius.all(
                            Radius.circular(size.width * 0.08))),
                    child: Row(
                      children: [
                        (size.width * 0.05).pw,
                        ElevatedButton(
                            onPressed: () {},
                            style: ElevatedButton.styleFrom(
                              primary: AppColors.mainGreyColor,
                              fixedSize:
                                  Size(size.width * 0.2, size.width * 0.25),
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.circular(size.width * 0.05),
                              ),
                            ),
                            child: Container()),
                        (size.width * 0.05).pw,
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            (size.width * 0.045).ph,
                            CustomText(
                              content: "Almhyar Zahra",
                              fontWeight: FontWeight.bold,
                            ),
                            (size.width * 0.045).ph,
                            CustomText(
                              content: "Class XI-B | Roll no:04",
                              colorText: AppColors.mainGreyColor,
                              fontSize: size.width * 0.04,
                              fontWeight: FontWeight.w300,
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              bottom: size.width * 0.2,
                              left: size.width * 0.05),
                          child: IconButton(
                              onPressed: () {},
                              icon: Icon(
                                Icons.camera_alt_outlined,
                                color: AppColors.mainGreyColor,
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            isLoading
                ? Padding(
                    padding: EdgeInsets.only(top: size.width * 0.7),
                    child: Container(
                        width: size.width,
                        color: AppColors.mainWhiteColor,
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: size.width * 0.05),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              CustomText(content: collegeNameController.text),
                            ],
                          ),
                        )),
                  )
                : Padding(
                    padding: EdgeInsets.only(top: size.width * 0.7),
                    child: Container(
                      width: size.width,
                      //height: size.height,
                      color: AppColors.mainWhiteColor,
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: size.width * 0.05),
                          child: Column(children: [
                            CustomTextField(
                              controller: collegeNameController,
                              labelText: "College Name",
                              validator: (value) {
                                return value!.isEmpty || !isName(value)
                                    ? "Please Check Your College Name"
                                    : null;
                              },
                            ),
                            (size.width * 0.04).ph,
                            CustomTextField(
                              controller: sectionController,
                              labelText: "Section",
                              validator: (value) {
                                return value!.isEmpty || !isName(value)
                                    ? "Please Check Your Section"
                                    : null;
                              },
                            ),
                            (size.width * 0.04).ph,
                            CustomTextField(
                              controller: firstNameController,
                              labelText: "First Name",
                              suffixIcon: IconButton(
                                  onPressed: () {}, icon: Icon(Icons.lock)),
                              validator: (value) {
                                return value!.isEmpty || !isName(value)
                                    ? "Please Check Your First Name"
                                    : null;
                              },
                            ),
                            (size.width * 0.04).ph,
                            CustomTextField(
                              controller: lastNameController,
                              labelText: "Last Name",
                              suffixIcon: IconButton(
                                  onPressed: () {}, icon: Icon(Icons.lock)),
                              validator: (value) {
                                return value!.isEmpty || !isName(value)
                                    ? "Please Check Your Last Name"
                                    : null;
                              },
                            ),
                            (size.width * 0.04).ph,
                            CustomTextField(
                              controller: fatherNameController,
                              labelText: "Father Name",
                              suffixIcon: IconButton(
                                  onPressed: () {}, icon: Icon(Icons.lock)),
                              validator: (value) {
                                return value!.isEmpty || !isName(value)
                                    ? "Please Check Your Father Name"
                                    : null;
                              },
                            ),
                            (size.width * 0.04).ph,
                            CustomTextField(
                              controller: motherNameController,
                              labelText: "Mother Name",
                              suffixIcon: IconButton(
                                  onPressed: () {}, icon: Icon(Icons.lock)),
                              validator: (value) {
                                return value!.isEmpty || !isName(value)
                                    ? "Please Check Your Mother Name"
                                    : null;
                              },
                            ),
                            (size.width * 0.04).ph,
                            CustomTextField(
                              controller: idUniversityController,
                              labelText: "ID University",
                              suffixIcon: IconButton(
                                  onPressed: () {}, icon: Icon(Icons.lock)),
                              typeInput: TextInputType.number,
                              validator: (value) {
                                return value!.isEmpty || !isIDUniversity(value)
                                    ? "Please Check Your ID University"
                                    : null;
                              },
                            ),
                            (size.width * 0.04).ph,
                            CustomTextField(
                              controller: academicYearController,
                              labelText: "Starting With The Academic Year",
                              suffixIcon: IconButton(
                                  onPressed: () {}, icon: Icon(Icons.lock)),
                              typeInput: TextInputType.datetime,
                              validator: (value) {
                                return value!.isEmpty || !isTwoYear(value)
                                    ? "Please Check Your Academic Year"
                                    : null;
                              },
                            ),
                          ]),
                        ),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    ));
  }
}
