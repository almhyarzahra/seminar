import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_erp/utils/chat.dart';
import 'package:school_erp/utils/formatting.dart';

import '../../../models/models.dart';

class ChatListItem extends StatelessWidget {
  final UserModel user;

  final ChatEntity chat_item;
  final Function(ChatEntity) onPressed;

  ChatListItem(
      {Key? key,
      required this.user,
      required this.chat_item,
      required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(chat_item.name ?? getChatName(chat_item.participants, user)),
      subtitle: Row(children: [
        Expanded(
            child: Text(
          chat_item.last_message?.message ?? '..',
          maxLines: 1,
        )),
        Text(utcToLocal(chat_item.updated_at)),
      ]),
      onTap: () => onPressed(chat_item),
    );
  }
}
