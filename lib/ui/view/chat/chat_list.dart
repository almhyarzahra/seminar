import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pusher_client/pusher_client.dart';
import 'package:school_erp/cubit/cubet.dart';
import 'package:school_erp/models/models.dart';
import 'package:school_erp/ui/shared/custom_widget/chat_widget/startup_cotainer.dart';
import 'package:school_erp/ui/ui.dart';
import 'package:school_erp/ui/view/login_view/login_view.dart';
import 'package:school_erp/utils/laravel_echo/laravel_echo.dart';
import 'package:school_erp/utils/logger.dart';

import '../../../bloc/blocs.dart';
import 'chat_list_item.dart';

class ChatListScreen extends StatefulWidget {
  const ChatListScreen({Key? key}) : super(key: key);

  @override
  State<ChatListScreen> createState() => _ChatListScreenState();
}

class _ChatListScreenState extends State<ChatListScreen> {
  @override
  Widget build(BuildContext context) {
    var _authBloc = context.read<AuthBloc>();
    iLog(_authBloc.state.user);
    UserModel currentUser = _authBloc.state.user!;

    var _chatBloc = context.read<ChatBloc>();
    var _userBloc = context.read<UserBloc>();

    return StartUpContainer(
        onDispose: () {
          LaravelEcho.instance.disconnect();
        },
        onInit: () async {
          _chatBloc.add(ChatStarted());
          _userBloc.add(UserStarted());
          LaravelEcho.init(token: _authBloc.state.token!);
        },
        child: Scaffold(
          appBar: AppBar(actions: [
            BlocConsumer<AuthBloc, AuthState>(
                builder: (context, state) => IconButton(
                    onPressed: () {
                      // vLog();
                      context.read<GuestCubit>().signOut(context);
                    },
                    icon: Icon(Icons.logout)),
                listener: (context, state) {
                  if (!state.isAuthenticated)
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (_) => LoginView()));
                })
          ]),
          body: RefreshIndicator(
            onRefresh: () async {
              _chatBloc.add(ChatStarted());
              _userBloc.add(UserStarted());
            },
            child: BlocConsumer<ChatBloc, ChatState>(
              listener: (context, state) {},
              builder: (context, state) => (state.chats.isEmpty)
                  ? Text('NO thing')
                  : ListView.separated(
                      itemBuilder: (context, index) => ChatListItem(
                          user: currentUser,
                          chat_item: state.chats[index],
                          onPressed: (chat) {
                            _chatBloc.add(ChatSelected(chat));
                            Navigator.of(context).push(
                                MaterialPageRoute(builder: (_) => Chat()));
                          },
                          key: ValueKey(state.chats[index].id)),
                      itemCount: state.chats.length,
                      separatorBuilder: (_, __) => Divider(
                            height: 15,
                          )),
            ),
          ),
          floatingActionButton:
              BlocSelector<UserBloc, UserState, List<UserModel>>(
            builder: (context, state) {
              return FloatingActionButton(
                  onPressed: () {
                    showSearch(
                        context: context,
                        delegate: UserSearchDelegate(user: state));
                  },
                  child: Icon(Icons.add));
            },
            selector: (state) =>
                state.map(initial: (_) => [], loaded: (state) => state.users),
          ),
        ));
  }
}

class UserSearchDelegate extends SearchDelegate<UserModel?> {
  @override
  String get searchFieldLabel => "Search For Student";
  List<UserModel> user = [];

  UserSearchDelegate({required List<UserModel> user}) : user = user;

  @override
  ThemeData appBarTheme(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    return theme.copyWith(
        appBarTheme: AppBarTheme(
          color: Colors.yellow,
        ),
        hintColor: Colors.white,
        inputDecorationTheme:
            const InputDecorationTheme(border: InputBorder.none),
        textTheme: const TextTheme(
            titleLarge: TextStyle(
                color: Colors.white,
                fontSize: 18,
                fontWeight: FontWeight.bold)));
  }

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = "";
          },
          icon: const Icon(Icons.close)),
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () {
          // Navigator.of(context).pop();
          close(context, null);
        },
        icon: const Icon(Icons.navigate_before));
  }

  @override
  Widget buildResults(BuildContext context) {
    List<UserModel> listItems = query.isEmpty
        ? user
        : user
            .where((user) =>
                user.name.toLowerCase().startsWith(query.toLowerCase()))
            .toList();

    return listItems.isEmpty
        ? const Center(
            child: Text(
              "No Data Found !",
              style: TextStyle(color: Colors.black),
            ),
          )
        : Container(
            color: Colors.black,
            width: double.infinity,
            height: double.infinity,
            child: Container(
              // color: Colors.black,

              height: 290,
              width: double.infinity,
              child: ListView.builder(
                itemCount: listItems.length,
                itemBuilder: (context, index) {
                  return ListTile(
                      onTap: () async {
                        context
                            .read<ChatBloc>()
                            .add(UserSelected(user: listItems[index]));
                        // context.read<ChatBloc>().add(ChatSelected(chat));

                        Navigator.of(context)
                            .push(MaterialPageRoute(builder: (_) => Chat()));
                      },
                      leading: Container(
                        child: Text(
                          listItems[index].name,
                          style: TextStyle(color: Colors.white),
                        ),
                      ));
                },
              ),
            ),
          );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<UserModel> listItems = query.isEmpty
        ? user
        : user
            .where((user) =>
                user.name.toLowerCase().startsWith(query.toLowerCase()))
            .toList();
    return listItems.isEmpty
        ? const Center(
            child: Text(
              "No Data Found !",
              style: TextStyle(color: Colors.black),
            ),
          )
        : Container(
            color: Colors.black,
            width: double.infinity,
            height: double.infinity,
            child: Container(
              // color: Colors.black,

              height: 290,
              width: double.infinity,
              child: ListView.builder(
                itemCount: listItems.length,
                itemBuilder: (context, index) {
                  return ListTile(
                      onTap: () {},
                      leading: Container(
                        child: Text(
                          listItems[index].name,
                          style: TextStyle(color: Colors.white),
                        ),
                      ));
                },
              ),
            ),
          );
  }
}
