import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:school_erp/enums/message_type.dart';
import 'package:school_erp/repositories/user/user_repository.dart';
import 'package:school_erp/services/base_controller.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_toast.dart';
import 'package:school_erp/ui/view/verification_view/verification_view.dart';

class ResetPasswordController extends BaseController {
  TextEditingController emailController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  void emailVerification() {
    if (formKey.currentState!.validate()) {
      runFullLoadingFutureFunction(
          function: UserRepository()
              .verificationEmail(email: emailController.text)
              .then((value) {
        value.fold((l) {
          CustomToast.showMessage(
              message: "Email is Wrong", messageType: MessageType.REJECTED);
        }, (r) {
          CustomToast.showMessage(
              message: "The code has been sent to your email",
              messageType: MessageType.SUCCSESS);
          Get.off(VerificationView(
            email: emailController.text,
          ));
        });
      }));
    }
  }
}
