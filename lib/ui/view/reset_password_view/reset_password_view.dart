import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_button.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text_field.dart';
import 'package:school_erp/ui/shared/custom_widget/virtical_divider.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/view/reset_password_view/reset_password_controller.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';
import 'package:school_erp/ui/view/verification_view/verification_view.dart';

class ResetPasswordView extends StatefulWidget {
  const ResetPasswordView({super.key});

  @override
  State<ResetPasswordView> createState() => _ResetPasswordViewState();
}

class _ResetPasswordViewState extends State<ResetPasswordView> {
  ResetPasswordController controller = ResetPasswordController();
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColors.mainBlueColor,
      body: Form(
        key: controller.formKey,
        child: ListView(
          children: [
            (screenWidth(10)).ph,
            Center(
              child: CustomText(
                content: "Reset Password",
                colorText: AppColors.mainBlackColor,
                fontSize: screenWidth(10),
              ),
            ),
            (screenWidth(10)).ph,
            Center(
              child: CustomText(
                content:
                    "   Please enter your email to receive a\nlink to create a new password via email",
                colorText: AppColors.mainBlackColor,
                fontWeight: FontWeight.w300,
              ),
            ),
            (screenWidth(10)).ph,
            Container(
                height: screenHeight(1.4),
                decoration: BoxDecoration(
                    color: AppColors.mainWhiteColor,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(screenWidth(10)),
                      topRight: Radius.circular(screenWidth(10)),
                    )),
                child: Padding(
                  padding: EdgeInsetsDirectional.symmetric(
                      horizontal: screenWidth(10)),
                  child: Column(
                    children: [
                      (screenWidth(10)).ph,
                      CustomTextField(
                        controller: controller.emailController,
                        labelText: "Email",
                        validator: (value) {
                          return value!.isEmpty || !isEmail(value)
                              ? "Please Check Your Email"
                              : null;
                        },
                      ),
                      (screenWidth(10)).ph,
                      CustomButton(
                          onPressed: () {
                            controller.emailVerification();
                          },
                          Text: "SEND"),
                      Image.asset(
                        "images/Reset_password_rafiki.png",
                        width: screenWidth(1),
                        height: screenHeight(2.5),
                      ),
                      // (size.width * 0.9).ph,
                      VirticalDivider(),
                    ],
                  ),
                )),
          ],
        ),
      ),
    ));
  }
}
