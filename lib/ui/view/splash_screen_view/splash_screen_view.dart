import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:school_erp/bloc/auth/auth_bloc.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/ui.dart';
import 'package:school_erp/ui/view/login_view/login_view.dart';

class SplashScreenView extends StatefulWidget {
  const SplashScreenView({super.key});

  @override
  State<SplashScreenView> createState() => _SplashScreenViewState();
}

class _SplashScreenViewState extends State<SplashScreenView> {
  bool _isInit=false;
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    _initialize();

    super.didChangeDependencies();
  }

  void _initialize()async{
    if(!_isInit)
      Future.delayed(Duration(seconds: 1));
final _authState=context.read<AuthBloc>().state;
Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_)=>
    _authState.isAuthenticated?ChatListScreen():Container()));
_isInit=true;
  }


  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColors.mainBlueColor,
      body: ListView(
        children: [
          (size.height * 0.2).ph,
          Center(child: SvgPicture.asset("images/Logo.svg")),
          SpinKitThreeBounce(
            color: AppColors.mainWhiteColor,
          ),
          (size.height * 0.1).ph,
          SvgPicture.asset(
            "images/ImageLogo.svg",
            fit: BoxFit.fill,
          ),
        ],
      ),
    ));
  }
}

extension EmptyPadding on num {
  SizedBox get ph => SizedBox(height: toDouble());
  SizedBox get pw => SizedBox(width: toDouble());
}
