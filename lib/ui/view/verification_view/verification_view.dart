import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:school_erp/bloc/auth/auth_bloc.dart';
import 'package:school_erp/cubit/cubet.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_button.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text_field.dart';
import 'package:school_erp/ui/shared/custom_widget/virtical_divider.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/view/dashboard_view/dashboard_view.dart';
import 'package:school_erp/ui/view/login_view/login_view.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';
import 'package:school_erp/ui/view/verification_view/verification_controller.dart';

class VerificationView extends StatefulWidget {
  const VerificationView({super.key, required this.email});
  final String email;

  @override
  State<VerificationView> createState() => _VerificationViewState();
}

class _VerificationViewState extends State<VerificationView> {
  VerificationController controller = VerificationController();
  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColors.mainBlueColor,
      body: Form(
        key: controller.formKey,
        child: ListView(
          children: [
            (screenWidth(10)).ph,
            Center(
              child: CustomText(
                content: "Verification",
                colorText: AppColors.mainBlackColor,
                fontSize: screenWidth(10),
              ),
            ),
            (screenWidth(10)).ph,
            Center(
              child: CustomText(
                content:
                    "A 6 - Digit PIN has been sent to your email\n   address, enter it below to continue",
                colorText: AppColors.mainBlackColor,
                fontWeight: FontWeight.w300,
              ),
            ),
            (screenWidth(10)).ph,
            Container(
                height: screenHeight(1.4),
                decoration: BoxDecoration(
                    color: AppColors.mainWhiteColor,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(screenWidth(10)),
                      topRight: Radius.circular(screenWidth(10)),
                    )),
                child: Padding(
                  padding: EdgeInsetsDirectional.symmetric(
                      horizontal: screenWidth(10)),
                  child: Column(
                    children: [
                      (screenWidth(5)).ph,
                      Container(
                        child: PinCodeTextField(
                          keyboardType: TextInputType.number,
                          appContext: context,
                          controller: controller.numberController,
                          length: 6,
                          cursorHeight: screenWidth(15),
                          enableActiveFill: true,
                          textStyle: TextStyle(fontSize: screenWidth(20)),
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          pinTheme: PinTheme(
                            shape: PinCodeFieldShape.box,
                            fieldWidth: screenWidth(8),
                            fieldHeight: screenWidth(8),
                            inactiveColor: Colors.grey,
                            selectedColor: Colors.lightBlue,
                            activeColor: Colors.blue,
                            selectedFillColor: Colors.blue,
                            inactiveFillColor: Colors.grey.shade100,
                            borderWidth: 1,
                            borderRadius: BorderRadius.circular(8),
                          ),
                          validator: (value) {
                            return value!.isEmpty || value.length < 6
                                ? "Please Enter Your Verification Number"
                                : null;
                          },
                          onChanged: ((value) {
                            controller.showTextField();
                          }),
                        ),
                      ),
                      (screenWidth(10)).ph,
                      Obx(() {
                        return Visibility(
                          visible: controller.show.value,
                          child: CustomTextField(
                            controller: controller.passwordController,
                            labelText: "New Password",
                            Obscure: controller.hidden.value,
                            fontWeight: FontWeight.bold,
                            letterSpacing: screenWidth(20),
                            suffixIcon: IconButton(
                                onPressed: () {
                                  controller.replace();
                                },
                                icon: Icon(controller.hidden.isTrue
                                    ? Icons.visibility_off_outlined
                                    : Icons.visibility)),
                            validator: (value) {
                              return value!.isEmpty
                                  // || !isPassword(value)
                                  ? "Please Check Your Password"
                                  : null;
                            },
                          ),
                        );
                      }),
                      (screenWidth(15)).ph,
                      CustomButton(
                          onPressed: () {
                            controller.resetPassword(widget.email);
                          },
                          Text: "CONTINUE"),
                      Image.asset(
                        "images/Confirmed-rafiki.png",
                        height: screenHeight(4),
                      ),
                      // (size.width * 0.65).ph,
                      VirticalDivider(),
                    ],
                  ),
                )),
          ],
        ),
      ),
    ));
  }
}
