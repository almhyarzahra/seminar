import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:school_erp/enums/message_type.dart';
import 'package:school_erp/repositories/repositories.dart';
import 'package:school_erp/services/base_controller.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_toast.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/ui/view/login_view/login_view.dart';

class VerificationController extends BaseController {
  TextEditingController numberController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  RxBool hidden = true.obs;
  RxBool show = false.obs;

  void replace() {
    hidden.isTrue ? hidden.value = false : hidden.value = true;
  }

  bool showTextField() {
    numberController.text.length >= 5 ? show.value = true : show.value = false;
    return show.value;
  }

  void resetPassword(String email) {
    runFullLoadingFutureFunction(
        function: UserRepository()
            .resetPassword(
                email: email,
                password: passwordController.text,
                token: numberController.text)
            .then((value) {
      value.fold((l) {
        CustomToast.showMessage(message: l, messageType: MessageType.REJECTED);
      }, (r) {
        CustomToast.showMessage(
            message: "Succsess", messageType: MessageType.SUCCSESS);
        store.setTokenInfo(r);
        Get.off(LoginView());
      });
    }));
  }
}
