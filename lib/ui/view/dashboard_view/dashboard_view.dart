import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:school_erp/ui/shared/colors.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_button_vertical.dart';
import 'package:school_erp/ui/shared/custom_widget/custom_text.dart';
import 'package:school_erp/ui/view/profile_view/profile_view.dart';
import 'package:school_erp/ui/view/splash_screen_view/splash_screen_view.dart';

class DashboardView extends StatefulWidget {
  const DashboardView({super.key});

  @override
  State<DashboardView> createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColors.mainBlueColor,
      body: Stack(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: size.width * 0.05),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                (size.width * 0.1).ph,
                CustomText(
                  content: "Hi Almhyar",
                  colorText: AppColors.mainWhiteColor,
                  fontSize: size.width * 0.07,
                  fontWeight: FontWeight.bold,
                ),
                (size.width * 0.03).ph,
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomText(
                      content: "Class XI-B | Roll no:04",
                      colorText: AppColors.mainWhiteColor,
                      fontWeight: FontWeight.w300,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ProfileView(),
                            ));
                      },
                      child: Ink(
                        height: size.width * 0.15,
                        width: size.width * 0.15,
                        decoration: BoxDecoration(
                            color: AppColors.mainGreyColor,
                            border: Border.all(color: AppColors.mainWhiteColor),
                            borderRadius:
                                BorderRadius.all(Radius.circular(size.width))),
                      ),
                    ),
                  ],
                ),
                Container(
                    width: size.width * 0.3,
                    height: size.width * 0.08,
                    decoration: BoxDecoration(
                        color: AppColors.mainWhiteColor,
                        borderRadius:
                            BorderRadius.all(Radius.circular(size.width))),
                    child: Center(
                      child: CustomText(
                        content: "2022-2023",
                        colorText: AppColors.mainBlueColor,
                      ),
                    )),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: size.width * 0.37),
            child: SvgPicture.asset('images/star_pattern.svg'),
          ),
          Padding(
            padding: EdgeInsets.only(top: size.width * 0.65),
            child: Container(
              height: size.height,
              width: size.width,
              decoration: BoxDecoration(
                  color: AppColors.mainWhiteColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(size.width * 0.1),
                    topRight: Radius.circular(size.width * 0.1),
                  )),
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.only(top: size.width * 0.4),
                  child: Row(
                    children: [
                      Expanded(
                          child: Column(
                        children: [
                          CustomButtonVertical(
                              onPressed: () {},
                              svgName: "ic_quiz",
                              Text: "Play Quiz"),
                          (size.width * 0.05).ph,
                          CustomButtonVertical(
                              onPressed: () {},
                              svgName: "ic_holiday",
                              Text: "School Holiday"),
                          (size.width * 0.05).ph,
                          CustomButtonVertical(
                              onPressed: () {},
                              svgName: "ic_results",
                              Text: "Result       "),
                          (size.width * 0.05).ph,
                          CustomButtonVertical(
                              onPressed: () {},
                              svgName: "ic_doubts",
                              Text: "Ask Doubts"),
                          (size.width * 0.05).ph,
                          CustomButtonVertical(
                              onPressed: () {},
                              svgName: "ic_leave",
                              Text: "Leave App"),
                          (size.width * 0.05).ph,
                          CustomButtonVertical(
                              onPressed: () {},
                              svgName: "ic_event",
                              Text: "Events      "),
                          (size.width * 0.05).ph,
                        ],
                      )),
                      Expanded(
                          child: Column(
                        children: [
                          CustomButtonVertical(
                              onPressed: () {},
                              svgName: "ic_assignment",
                              Text: "Assignment"),
                          (size.width * 0.05).ph,
                          CustomButtonVertical(
                              onPressed: () {},
                              svgName: "ic_calendra",
                              Text: "Time Table"),
                          (size.width * 0.05).ph,
                          CustomButtonVertical(
                              onPressed: () {},
                              svgName: "ic_date_sheet",
                              Text: "Date Sheet"),
                          (size.width * 0.05).ph,
                          CustomButtonVertical(
                              onPressed: () {},
                              svgName: "ic_gallery",
                              Text: "School Gallery"),
                          (size.width * 0.05).ph,
                          CustomButtonVertical(
                              onPressed: () {},
                              svgName: "ic_password",
                              Text: "Change Pass"),
                          (size.width * 0.05).ph,
                          CustomButtonVertical(
                              onPressed: () {},
                              svgName: "ic_logout",
                              Text: "Logout         "),
                          (size.width * 0.05).ph,
                        ],
                      )),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            top: size.width * 0.5,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: size.width * 0.02),
              child: Row(
                children: [
                  ElevatedButton(
                      onPressed: () {},
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: size.width * 0.05, right: size.width * 0.1),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SvgPicture.asset("images/ic_attendance.svg"),
                            (size.width * 0.04).ph,
                            CustomText(
                              content: "80.39%",
                              fontSize: size.width * 0.08,
                            ),
                            (size.width * 0.02).ph,
                            CustomText(
                              content: "Attendance",
                              colorText: AppColors.mainGreyColor,
                            ),
                          ],
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                        primary: AppColors.mainWhiteColor,
                        fixedSize: Size(size.width * 0.47, size.width * 0.5),
                        side: BorderSide(color: AppColors.mainGreyColor),
                        shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(size.width * 0.05),
                        ),
                      )),
                  (size.width * 0.035).pw,
                  ElevatedButton(
                      onPressed: () {},
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: size.width * 0.05, right: size.width * 0.1),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SvgPicture.asset("images/ic_fees_due.svg"),
                            (size.width * 0.04).ph,
                            CustomText(
                              content: "\$6400",
                              fontSize: size.width * 0.08,
                            ),
                            (size.width * 0.02).ph,
                            CustomText(
                              content: "Fees Due",
                              colorText: AppColors.mainGreyColor,
                            ),
                          ],
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                        primary: AppColors.mainWhiteColor,
                        fixedSize: Size(size.width * 0.47, size.width * 0.5),
                        side: BorderSide(color: AppColors.mainGreyColor),
                        shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(size.width * 0.05),
                        ),
                      )),
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
