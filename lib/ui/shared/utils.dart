import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:school_erp/repositories/shared_preference_repository.dart';
import 'package:school_erp/ui/shared/colors.dart';

bool isEmail(String value) {
  RegExp regExp = new RegExp(
      (r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+"));
  return regExp.hasMatch(value);
}

bool isPassword(String value) {
  RegExp regExp = new RegExp(
      r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$');
  if (value.length < 10) {
    return false;
  } else
    return regExp.hasMatch(value);
}

bool isName(String value) {
  RegExp regExp = new RegExp(r'^[a-zA-Z]+$');

  return regExp.hasMatch(value);
}

bool isIDUniversity(String value) {
  RegExp regExp = new RegExp(r'^\d{5}$');

  return regExp.hasMatch(value);
}

bool isTwoYear(String value) {
  RegExp regExp = new RegExp(r'^\d{4}\/\d{4}$');

  return regExp.hasMatch(value);
}

double screenWidth(double perecent) {
  return Get.size.width / perecent;
}

double screenHeight(double perecent) {
  return Get.size.height / perecent;
}

void customLoader() => BotToast.showCustomLoading(toastBuilder: (builder) {
      return Container(
        width: screenWidth(5),
        height: screenWidth(5),
        decoration: BoxDecoration(
            color: AppColors.mainBlackColor.withOpacity(0.5),
            borderRadius: BorderRadius.circular(15)),
        child: SpinKitCircle(color: AppColors.mainBlueColor),
      );
    });

SharedPreferenceRepository get store => Get.find<SharedPreferenceRepository>();
