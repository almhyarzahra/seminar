import 'package:flutter/material.dart';
import 'package:school_erp/ui/shared/colors.dart';

class CustomTextField extends StatefulWidget {
  const CustomTextField(
      {super.key,
      this.Obscure,
      this.letterSpacing,
      this.fontWeight,
      required this.controller,
      this.validator,
      required this.labelText,
      this.suffixIcon,
      this.typeInput});

  final bool? Obscure;
  final double? letterSpacing;
  final FontWeight? fontWeight;
  final TextEditingController controller;
  final String labelText;
  final IconButton? suffixIcon;
  final TextInputType? typeInput;

  final String? Function(String?)? validator;

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return TextFormField(
      keyboardType: widget.typeInput ?? TextInputType.text,
      validator: widget.validator,
      controller: widget.controller,
      textInputAction: TextInputAction.next,
      obscureText: widget.Obscure ?? false,
      style: TextStyle(
          letterSpacing: widget.letterSpacing ?? 0,
          fontWeight: widget.fontWeight ?? FontWeight.w400,
          color: AppColors.mainBlackColor,
          fontSize: size.width * 0.055),
      decoration: InputDecoration(
          suffixIcon: widget.suffixIcon ?? null,
          labelText: widget.labelText,
          labelStyle: TextStyle(
              color: AppColors.mainGreyColor,
              fontSize: size.width * 0.05,
              letterSpacing: 0),
          errorStyle: TextStyle(color: AppColors.mainRedColor),
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: AppColors.mainBlueColor))),
    );
  }
}
