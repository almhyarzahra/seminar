import 'package:flutter/material.dart';
import 'package:school_erp/ui/shared/colors.dart';

class CustomText extends StatelessWidget {
  const CustomText({
    super.key,
    required this.content,
    this.colorText,
    this.fontSize,
    this.fontWeight,
    this.decoration,
    this.decorationThickness,
    this.textAlign,
  });
  final String content;
  final Color? colorText;
  final double? fontSize;
  final FontWeight? fontWeight;
  final TextDecoration? decoration;
  final double? decorationThickness;
  final TextAlign? textAlign;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Text(
      content,
      textAlign: textAlign,
      style: TextStyle(
        color: colorText ?? AppColors.mainBlackColor,
        fontSize: fontSize ?? size.width * 0.045,
        fontWeight: fontWeight ?? FontWeight.w400,
        decoration: decoration ?? TextDecoration.none,
        decorationThickness: decorationThickness ?? 0,
      ),
    );
  }
}
