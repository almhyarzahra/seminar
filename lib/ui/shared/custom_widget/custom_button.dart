import 'package:flutter/material.dart';

import 'package:school_erp/ui/shared/colors.dart';

class CustomButton extends StatefulWidget {
  const CustomButton({
    super.key,
    required this.onPressed,
    required this.Text,
    this.backgroundColor,
    this.TextColor,
    this.suffixIcon,
  });
  final VoidCallback onPressed;
  final String Text;
  final Color? backgroundColor;
  final IconData? suffixIcon;
  final Color? TextColor;

  @override
  State<CustomButton> createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return ElevatedButton(
        onPressed: () {
          widget.onPressed();
        },
        style: ElevatedButton.styleFrom(
          shape: StadiumBorder(),
          backgroundColor: widget.backgroundColor ?? AppColors.mainBlueColor,
          fixedSize: Size(size.width * 0.85, size.height * 0.06),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (widget.suffixIcon != null) ...[
              Expanded(
                child: Center(
                  child: Text(
                    widget.Text,
                    style: TextStyle(
                      color: widget.TextColor ?? AppColors.mainWhiteColor,
                    ),
                  ),
                ),
              ),
              Icon(
                widget.suffixIcon,
                color: AppColors.mainBlackColor,
              ),
            ] else
              Text(
                widget.Text,
                style: TextStyle(
                  color: widget.TextColor ?? AppColors.mainWhiteColor,
                ),
              ),
          ],
        ));
  }
}
