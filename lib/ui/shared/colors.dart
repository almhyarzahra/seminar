import 'package:flutter/material.dart';

class AppColors {
  static Color mainBlueColor = Color.fromRGBO(121, 224, 238, 1);
  static Color mainWhiteColor = Color.fromRGBO(255, 255, 255, 1);
  static Color mainBlackColor = Color.fromRGBO(0, 0, 0, 1);
  static Color mainGreyColor = Color.fromRGBO(165, 165, 165, 1);
  static Color mainGrey2Color = Color.fromRGBO(245, 246, 252, 1);
  static Color mainRedColor = Color.fromRGBO(221, 75, 57, 1);
  static Color mainOrangeColor = Color.fromRGBO(252, 96, 17, 1);
  static Color mainColorGreen = Color.fromRGBO(0, 197, 105, 1);
  static Color mainPurpleBackGroundColor = Color.fromRGBO(252, 226, 182, 1);
  static Color mainBlueBackGroundColor = Color.fromRGBO(220, 240, 249, 1);
  static Color mainBlue100BackGroundColor = Color.fromRGBO(217, 228, 246, 1);
  static Color mainYellowColor = Color.fromRGBO(245, 158, 11, 1);
  static Color mainBlue10Color = Color.fromRGBO(82, 182, 223, 1);
  static Color mainBlue20Color = Color.fromRGBO(65, 120, 212, 1);
}
