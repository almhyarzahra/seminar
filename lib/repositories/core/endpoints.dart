import 'package:school_erp/repositories/core/network_config.dart';

class Endpoints {
  static const _apiVersion = 'api';

  //Auth
  static const _baseAuth = "$_apiVersion/auth";

  static const login = "$_apiVersion/sanctum/token";
// static const login="$_apiVersion/login";
  static const loginWithToken = "$_apiVersion/login_with_token";
  static const logout = "$_apiVersion/user/revoke";
  static const allUser = "$_apiVersion/allUser";

  //Chat
  static const _baseChat = "$_apiVersion/chat";

  static const getChats = "$_apiVersion/chat";
  static const getSingleChat = "$_baseChat/chat";
  static const createChat = "$_apiVersion/chat";

  //Chat Message
  static const _baseChatMessage = "$_apiVersion/chat_message";
  static const getChatMessage = "$_apiVersion/message";
  static const createChatMessage = "$_apiVersion/message";

//User
  static const getUsers = "$_apiVersion/user";

  static String forget_password = NetworkConfig.getFulApiUrl('forgot-password');
  static String reset_password = NetworkConfig.getFulApiUrl('reset-Password');
  static String subject = NetworkConfig.getFulApiUrl('subject');
}
