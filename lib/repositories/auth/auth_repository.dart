import 'dart:developer';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:school_erp/models/app_response.dart';
import 'package:school_erp/models/requests/login_request.dart';
import 'package:school_erp/models/requests/register_request.dart';
import 'package:school_erp/models/user_model.dart';
import 'package:school_erp/repositories/auth/base_auth_repository.dart';
import 'package:school_erp/repositories/core/endpoints.dart';
import 'package:school_erp/utils/dio_client/dio_client.dart';
import 'package:school_erp/utils/logger.dart';

class AuthRepository extends BaseAuthRepository {
  AuthRepository({Dio? dioClient})
      : _dioClient = dioClient ?? DioClient().instance;
  final Dio _dioClient;

  @override
  Future<AppResponse<AuthUser?>> login(LoginRequest request) async {
    eLog(request.toJson());
    final respone = await _dioClient.post("api/sanctum/token",
        queryParameters: request.toJson());
    iLog(respone.data);
    return AppResponse<AuthUser?>.fromJson(
        respone.data,
        (dynamic json) => respone.data['success'] && json != null
            ? AuthUser.fromJson(json)
            : null);
  }

  @override
  Future<AppResponse<UserModel?>> loginWithToken() async {
    // TODO: implement loginWithToken

    final respone = await _dioClient.post(Endpoints.loginWithToken);
    return AppResponse<UserModel?>.fromJson(
        respone.data,
        (dynamic json) => respone.data['success'] && json != null
            ? UserModel.fromJson(json)
            : null);
  }

  @override
  Future<AppResponse> logout({required String token}) async {
    // TODO: implement logout

    _dioClient.options.headers[HttpHeaders.authorizationHeader] =
        'Bearer $token';
    final respone = await _dioClient.get("/api/user/revoke");

    return AppResponse.fromJson({
      "statusMessage": respone.data.toString(),
      "statusCode": 200,
      "success": true,
      "message": "message"
    }, (dynamic json) => null);
  }
//
// @override
// Future<AppResponse<AuthUser?>> register(RegisterRequest request) async {
//
// }
}
