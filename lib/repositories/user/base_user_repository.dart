import 'package:school_erp/models/apis/forget_password_model.dart';
import 'package:school_erp/models/apis/subject_model.dart';
import 'package:school_erp/models/apis/token_info.dart';
import 'package:school_erp/models/models.dart';
import 'package:dartz/dartz.dart';

abstract class BaseUserRepository {
  Future<AppResponse<List<UserModel>>> getUsers();
  Future<Either<String, ForgetPasswordModel>> verificationEmail(
      {required String email});
  Future<Either<String, TokenInfo>> resetPassword({
    required String email,
    required String password,
    required String token,
  });
  Future<Either<String, SubjectModel>> getSubjectUser();
}
