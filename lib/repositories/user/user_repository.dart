import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:school_erp/bloc/auth/auth_bloc.dart';
import 'package:school_erp/enums/request_type.dart';
import 'package:school_erp/models/apis/forget_password_model.dart';
import 'package:school_erp/models/apis/subject_model.dart';
import 'package:school_erp/models/apis/token_info.dart';
import 'package:school_erp/models/app_response.dart';
import 'package:school_erp/models/common_response.dart';
import 'package:school_erp/models/user_model.dart';
import 'package:school_erp/repositories/core/network_config.dart';
import 'package:school_erp/repositories/user/base_user_repository.dart';
import 'package:school_erp/utils/logger.dart';
import 'package:school_erp/utils/network_util.dart';

import '../../utils/dio_client/dio_client.dart';
import '../core/endpoints.dart';

class UserRepository extends BaseUserRepository {
  final Dio _dioClient;

  UserRepository({Dio? dioClient})
      : _dioClient = dioClient ?? DioClient().instance;

  @override
  Future<AppResponse<List<UserModel>>> getUsers() async {
    _dioClient.options.headers[HttpHeaders.authorizationHeader] =
        'Bearer ${AuthBloc().state.token}';

    var response = await _dioClient.get(Endpoints.allUser);
    return AppResponse<List<UserModel>>.fromJson(response.data, (dynamic json) {
      if (response.data['success'] && json != null) {
        return (json['user'] as List<dynamic>)
            .map((e) => UserModel.fromJson(e))
            .toList();
      }
      return [];
    });
  }

  @override
  Future<Either<String, ForgetPasswordModel>> verificationEmail(
      {required String email}) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.POST,
        url: Endpoints.forget_password,
        params: {'email': email},
        headers: NetworkConfig.getHeaders(
          needAuth: false,
        ),
      ).then((response) {
        eLog(response);
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(ForgetPasswordModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String, TokenInfo>> resetPassword({
    required String email,
    required String password,
    required String token,
  }) async {
    try {
      return NetworkUtil.sendRequest(
          type: RequestType.POST,
          url: Endpoints.reset_password,
          headers: NetworkConfig.getHeaders(
            needAuth: false,
          ),
          body: {
            "email": email,
            "password": password,
            "token": token,
          }).then((response) {
        eLog(response);
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(TokenInfo.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String, SubjectModel>> getSubjectUser() async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: Endpoints.subject,
        headers: NetworkConfig.getHeaders(
          needAuth: true,
        ),
      ).then((response) {
        eLog(response);
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(SubjectModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(commonResponse.message ?? '');
        }
      });
    } catch (e) {
      return Left(e.toString());
    }
  }
}
