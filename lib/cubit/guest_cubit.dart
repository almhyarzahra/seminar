import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_login/flutter_login.dart';
import 'package:meta/meta.dart';
import 'package:school_erp/bloc/test_auth/test_auth_bloc.dart';
import 'package:school_erp/models/models.dart';
import 'package:school_erp/ui/shared/utils.dart';
import 'package:school_erp/utils/utils.dart';

import '../bloc/auth/auth_bloc.dart';
import '../models/requests/login_request.dart';
import '../models/requests/register_request.dart';
import '../repositories/auth/auth_repository.dart';

part 'guest_state.dart';

class GuestCubit extends Cubit<GuestState> {
  final AuthRepository _authRrepository;
  final AuthBloc _authBloc;

  GuestCubit(
      {required AuthRepository authRrepository, required AuthBloc authBloc})
      : _authRrepository = authRrepository,
        _authBloc = authBloc,
        super(GuestInitial());

  Duration get loginTime => Duration(milliseconds: 2250);

  Future<String?> signIn(
      {required UserModel user, required BuildContext context}) async {
    AppResponse<AuthUser?> response = await _authRrepository
        .login(LoginRequest(email: user.email, password: user.password));
// if(response.success)
    eLog(response.data);
    if (response.success) {
      BlocProvider.of<AuthBloc>(context).add(Authenticated(
          isAuthenticated: true,
          user: response.data!.user,
          token: response.data!.token));
      store.setTokenInfoString(response.data!.token);

      return null;
    }
    return response.message;
  }

  // Future<String?> signUp(UserModel user) async {
  //   log("response.data.toString()");
  //
  //   var response = await _authRrepository.register(RegisterRequest(
  //       email: user.email,
  //       password: user.password,
  //       number: user.number,
  //       image_url: user.image_url));
  //   log(response.data.toString());
  //
  //   if (response.success) {
  //     _authBloc.add(Authenticated(
  //         isAuthenticated: true,
  //         user: response.data!.user,
  //         token: response.data!.token));
  //     return null;
  //   }
  //   return response.message;
  // }

  Future<void> signOut(context) async {
    BlocProvider.of<AuthBloc>(context)
        .add(Authenticated(isAuthenticated: false));

    var response = await _authRrepository.logout(token: _authBloc.state.token!);
  }
}
