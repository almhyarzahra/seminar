import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:school_erp/bloc/chat/chat_bloc.dart';
import 'package:school_erp/bloc/user/user_bloc.dart';
import 'package:school_erp/ui/view/home_student_view/home_student_view.dart';
import 'package:school_erp/ui/view/intro_view/intro_view.dart';
import 'package:school_erp/ui/view/login_view/login_view.dart';
import 'package:school_erp/utils/logger.dart';

import '../bloc/auth/auth_bloc.dart';
import '../bloc/test_auth/test_auth_bloc.dart';
import '../cubit/guest_cubit.dart';
import '../repositories/repositories.dart';
import '../ui/view/chat/chat_list.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/routes/transitions_type.dart'
    as Transition;

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
        providers: [
          RepositoryProvider<ChatMessageRepository>(
              create: (_) => ChatMessageRepository()),
          RepositoryProvider<ChatRepository>(create: (_) => ChatRepository()),
          RepositoryProvider<AuthRepository>(create: (_) => AuthRepository()),
          RepositoryProvider<UserRepository>(create: (_) => UserRepository())
        ],
        child: MultiBlocProvider(
          providers: [
            BlocProvider(create: (_) => AuthBloc()),
            BlocProvider(create: (_) => TestAuthBloc()),
            BlocProvider(
                create: (_) => ChatBloc(
                    chatRepository: ChatRepository(),
                    chatMessageRepository: ChatMessageRepository())),
            BlocProvider(
                create: (_) => GuestCubit(
                    authBloc: AuthBloc(), authRrepository: AuthRepository())),
            BlocProvider(
                create: (_) => UserBloc(userRepository: UserRepository()))
          ],
          child: BlocConsumer<AuthBloc, AuthState>(
            listener: (context, state) {
              // TODO: implement listener
            },
            builder: (context, state) {
              return GetMaterialApp(
                  defaultTransition: GetPlatform.isAndroid
                      ? Transition.Transition.fadeIn
                      : Transition.Transition.cupertino,
                  transitionDuration: Duration(milliseconds: 300),
                  routes: {'login_view': (_) => LoginView()},
                  builder: BotToastInit(),
                  navigatorObservers: [BotToastNavigatorObserver()],
                  themeMode: ThemeMode.dark,
                  darkTheme: ThemeData(
                    brightness: Brightness.light,
                    /* dark theme settings */
                  ),
                  debugShowCheckedModeBanner: false,
                  home: context.read<AuthBloc>().state.isAuthenticated
                      ? HomeStudentView()
                      : IntroView());
            },
          ),
        ));
  }
}
