part of 'chat_bloc.dart';

@freezed
class ChatState with _$ChatState {
  const ChatState._();
  factory ChatState.initial() {
    return ChatState(
        chatMessages: [],
        isLastPage: false,
        page: 1,
        chats: [],
        selectedChat: null,
        status: DataStatus.initial,
        message: "",
        otherUserId: null);
  }

  const factory ChatState(
      {required int page,
      required bool isLastPage,
      required List<ChatMessageEntity> chatMessages,
      required List<ChatEntity> chats,
      required String message,
      ChatEntity? selectedChat,
      required DataStatus status,
      int? otherUserId}) = _ChatState;

bool get isSearchChat =>otherUserId!=null && selectedChat==null;
bool get isListChat =>otherUserId==null && selectedChat!=null;
List<ChatMessage> get getChatMessage{
  return chatMessages.map((e) => e.getChatMessage).toList();
}
}
