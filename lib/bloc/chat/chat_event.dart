part of 'chat_bloc.dart';

@freezed
class ChatEvent with _$ChatEvent {
  const factory ChatEvent.started() = ChatStarted;

  const factory ChatEvent.reset({bool? shouldResetChat}) = ChatReset;

  const factory ChatEvent.userSelected({required UserModel user}) =
      UserSelected;

  const factory ChatEvent.getChatMessage() = GetChateMessage;

  const factory ChatEvent.loadMoreChatMessage() = LoadMoreChatMessage;

  const factory ChatEvent.chatSelected(ChatEntity chat) = ChatSelected;

  const factory ChatEvent.sendMessage(
      {required int chatId, required ChatMessage message,required String socketId}) = SendMessage;

  const factory ChatEvent.addNewMessage({required ChatMessageEntity message}) =
      AddNewMessage;
}
