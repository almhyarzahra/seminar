part of 'test_auth_bloc.dart';

@immutable
abstract class TestAuthState {}

class TestAuthInitial extends TestAuthState {}

class AuthState1 extends TestAuthState{
  bool isAuth;
  String token;
  UserModel user;

  AuthState1({required bool this.isAuth,required String this.token,required UserModel this.user});



}
class NotAuthState extends TestAuthState{
  bool isAuth;
  String? token;
  UserModel? user;

  NotAuthState({required bool this.isAuth,required String? this.token,required UserModel? this.user});



}

