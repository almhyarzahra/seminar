import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:meta/meta.dart';
import 'package:school_erp/models/models.dart';

part 'test_auth_event.dart';
part 'test_auth_state.dart';

class TestAuthBloc extends Bloc<TestAuthEvent, TestAuthState> {
  TestAuthBloc() : super(TestAuthInitial()) {
    on<TestAuthEvent>((event, emit) {
      if(event is
      NotAuth)
      emit(NotAuthState(isAuth: event.isAuth, token: event.token, user: event.user));
      else if (event is Auth)
        emit(AuthState1(isAuth: event.isAuth, token: event.token, user: event.user));

      // TODO: implement event handler
    });

  }

  // @override
  // TestAuthState? fromJson(Map<String, dynamic> json) {
  //   // TODO: implement fromJson
  //   throw UnimplementedError();
  // }

  // @override
  // Map<String, dynamic>? toJson(TestAuthState state) {
  //   // TODO: implement toJson
  //   throw UnimplementedError();
  // }
}
